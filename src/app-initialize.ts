// Core Ionic types
// tslint:disable-next-line:no-import-side-effect
import { IonicConfig } from '@ionic/core';

// Webpack import for ionicons
import { addIcons } from 'ionicons';
//import { arrowBack, arrowDown, arrowForward, close, closeCircle, menu, reorder, search } from 'ionicons/icons';
import { arrowBack, arrowDown, arrowForward, close, closeCircle, menu, search } from 'ionicons/icons';

// import '@ionic/core/css/ionic.bundle.css';
// import 'ionicons/dist/collection/icon/icon.css';

import { applyPolyfills, defineCustomElements } from '@ionic/core/loader';
import { IonicWindow } from './interfaces';

export function appInitialize(config?: IonicConfig) {
  const win: IonicWindow = window as any;
  const Ionic = (win.Ionic = win.Ionic || {});

  Ionic.config = config;
  applyPolyfills().then(() => defineCustomElements(win));

  // Icons that are used by internal components
  addIcons({
    'ios-close': close,
    'md-close': close,
    // 'ios-reorder': reorder,
    // 'md-reorder': reorder.md,
    'ios-menu': menu,
    'md-menu': menu,
    'ios-arrow-forward': arrowForward,
    'md-arrow-forward': arrowForward,
    'ios-arrow-back': arrowBack,
    'md-arrow-back': arrowBack,
    'ios-arrow-down': arrowDown,
    'md-arrow-down': arrowDown,
    'ios-search': search,
    'md-search': search,
    'ios-close-circle': closeCircle,
    'md-close-circle': closeCircle,
  });
}
